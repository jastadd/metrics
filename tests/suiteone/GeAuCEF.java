package g;

import abs.A;
import c.CeAuB;
import f.FeE2;

public class GeAuCEF extends A {

	private static boolean used = false;

	public String dummy() {
		String s = super.dummy();
		if (!used) {
			CeAuB c = new CeAuB();
			FeE2 e = new FeE2();
			System.out.println("G using " + c.dummy() + " and " + e.foo());
			used = true;
		}
		return s + " --> Dummy G";
	}
}
