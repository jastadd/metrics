package main;

import b.BeA;
import c.CeAuB;
import d.DuC;
import f.FeE2;
import f.FeF;
import g.GeAuCEF;
import g.GeE;

public class Main {
	
	public static void main(String[] args) {
		BeA b = new BeA();
		CeAuB c = new CeAuB();
		DuC d = new DuC();
		FeF f = new FeF();
		FeE2 f2 = new FeE2();
		GeE g = new GeE();
		GeAuCEF g2 = new GeAuCEF();
		System.out.println(b.dummy());
		System.out.println(c.dummy());
		System.out.println(d.bar());
		System.out.println(f.foo());
		System.out.println(f2.foo());
		System.out.println(g.foo());
		System.out.println(g2.dummy());
	}
}
