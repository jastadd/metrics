package h;

import abs.A;
import b.BeA;

public class H extends A {

	public String dummy() {
		String s = super.dummy();
		s += " --> Dummy H";
		return s;
	}

	public class HH extends BeA {

		public String dummy() {	
			String s = super.dummy();
			s += " --> Dummy H";
			return s;
		}
	}
}
