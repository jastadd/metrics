package d;

import c.CeAuB;

public class DuC {

	private static boolean used = false;

	public DuC() {
		if (!used) {
			CeAuB o1, o2, o3;
			o1 = new CeAuB();
			o2 = new CeAuB();
			o3 = new CeAuB();
			System.out.println("D using " + o1.dummy() + " and " + o2.dummy()
					+ " and " + o3.dummy());
			o1.dummy();
			o2.dummy();
			o3.dummy();
			used = true;
		}
	}

	public String bar() {
		return "Bar DuC";
	}
}
