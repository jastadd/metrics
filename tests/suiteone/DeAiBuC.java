package d;

import abs.A;
import b.B;
import c.CiB;

public class DeAiBuC extends A implements B {

	private static boolean used = false;
	
		public DeAiBuC() {
			if(!used) {
			CiB o = new CiB();
			System.out.println("D using " + o.dummy());
			used = true;
			}
		}
		
		public String dummy(){
			String s = super.dummy();
			return s + " --> Dummy C implements B";
		}
}
