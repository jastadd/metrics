package c;

import abs.A;
import b.BeA;

public class CeAuB extends A {

	private static boolean used = false;

	public CeAuB() {
		if (!used) {
			BeA o = new BeA();
			System.out.println("C using " + o.dummy());
			used = true;
		}
	}

	public String dummy() {
		String s = super.dummy();
		return s + " --> Dummy C";
	}
}
