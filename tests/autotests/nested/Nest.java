package Nest;

import NoNest.*;

public class Nest {

	public void dummy() {
		int a = 17;
	}

	private class Nestled {

		public int dummy() {
			NoNest nn = new NoNest();
			nn.dummy();	
			int b = 5;
			return b+2;
		}
	}
}
