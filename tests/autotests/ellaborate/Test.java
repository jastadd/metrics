package main;

import foo.Foo;
import baz.Baz;
import norf.Norf;
import qux.Qux;
import peku.Peku;

public class Test {

	public static void main(String args[]) {
		Baz baz = new Baz();
		Norf norf = new Norf();
		Qux qux = new Qux();
		Foo foo = baz.foo();
		foo.foo();
		norf.foo();
		qux.foo();
		Peku.foo();
	}
}
