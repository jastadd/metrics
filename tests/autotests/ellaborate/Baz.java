package baz;

import foo.Foo;
import bar.Bar;

public class Baz {
	
	public Foo foo() {
		Bar bar = new Bar();
		return bar.foo();
	}
}
