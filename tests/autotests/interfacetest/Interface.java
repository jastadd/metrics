package interfaces;

public interface TestInterface {
	void methodA();
	int methodB(float a);
}
