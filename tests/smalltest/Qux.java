package quxnorf;

import foobar.*;

public class Qux {

	public static void main(String args[]) {
		Norf.bar();
		foo();
	}
	
	public static Foo foo() {
		return new Foo();
	}
}
