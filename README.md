Metrics calculation 1.0
========================

This project demonstrates a JastAddJ extension that calculates software
project metrics for a java project. It includes support for calculating
efferent and afferent couplings, instability and abstractness between
packages.

Use these commands to clone the project and build it the first time:

    git clone --recursive git@bitbucket.org:jastadd/metrics.git
    cd metrics
    gradle jar

Make sure to include the `--recursive` in the clone command to get the JastAddJ
submodule.

If you forgot the `--recursive` flag, don't worry, just go into the newly cloned
project and run these commands:

    git submodule init
    git submodule update

That should download the JastAddJ git repository into the local directory `jastaddj`.

If you don't have Gradle installed you can use the `gradlew.bat` (on Windows)
or `gradlew` (Unix) script instead. For example to build on Windows run the
following in a command prompt:

    gradlew jar

LICENSE
-------

See LICENSE.MD

Build and Run
-------------

    gradle jar
    java -jar metrics.jar tests/smalltest/*

This is assuming the project exists in a folder called metrics, otherwise the generated jar
will take the name of the current folder.

There is also a suite called junit which can be run.

Options
-------

The tool supports the following options:

-graph		: Generates .dot files which can be used to create graphs.
-list		: Prints the information to a file called "list"
-nostandard	: Causes the program to only calculate couplings within the compiled source.
-silent		: Suppresses output. Useful when you only want graphs

to run with any option just add them after the jar name: 
	java -jar metrics.jar -nostandard -graph -list -silent tests/smalltest/*

The tool also supports standard JastAddJ options.

Automated testing
-----------------

To create the testing framework, run:
	javac src/tests/TestMetrics.java

And to run the provided tests
	java -classpath src/tests/ TestMetrics

On linux systems, one can simply use the provided autotest file to save two lines of writing:

./autotest

NOTE: Due to the way values are stored internally, the order in which they are outputted may not always be consistent.
If this happens, manual inspection of the output of the tests is required. All the tests provided by us have been
checked to correspond to their expected values.

Building graphs
---------------

The .dot files are placed in the graphs directory.
There is a script called makedot that produces a ps file given a dot file.

Example:
	
	./makedot graphs/classes.dot

Which in turn gives you a file called classes.dot.ps which represents the class dependencies.
There is also a file called packages.dot which represents package dependencies.
