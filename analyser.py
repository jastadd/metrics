import numpy
from scipy import stats
import sys
minval = 1000
maxval = 0
tot = 0
temp = 0
vals = []
#Read values to vals. Calculate max and min
with open(sys.argv[1], 'r') as tool:
	#Sum results of two lines 
	for line in tool:
		#We don't care about real (line1)
		if "user" in line:
			temp = float(line.split()[1])
		if "sys" in line:
			tot = temp + float(line.split()[1])
			vals.append(tot)
			if tot > maxval:
				maxval = tot
			if tot < minval:
				minval = tot
print "Min " + str (minval)
print "Max " + str(maxval)
#Compute mean
mean = sum(vals)/len(vals)
#Standard deviation
stddev = numpy.std(vals)
#Sample size of 50 => Assume normal distribution
interval = stats.norm.interval(0.95, loc = mean, scale =stddev)
print "Mean " + str(mean)
print "Stddev " + str(stddev)
print interval
	
