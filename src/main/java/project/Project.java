/* Copyright (c) 2014, Olle Tervalampi-Olsson <dat11ote@student.lu.se>,
   Marcus Lacerda <dat11mla@student.lu.se>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Lund University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */


package project;

import AST.*;
import org.jastadd.jastaddj.*;
import java.util.*;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

/**
 * Compile Java programs.
 */
public class Project extends Frontend {

	/**
	 * Entry point for the compiler frontend.
	 * @param args command-line arguments
	 */
	public static void main(String args[]) {
		int exitCode = new Project().run(args);
		if (exitCode != 0) {
			System.exit(exitCode);
		}
	}

	private final JavaParser parser;
	private final BytecodeParser bytecodeParser;


	// Flag for not including standard library classes
	private	boolean noStandard;

	// Metrics
	private double instability = -1;
	private	double abstractness = -1;


	private	HashMap<String, LinkedList<Boolean>> absPackages;
	private	HashMap<String, LinkedList<String>> packages;
	private	HashMap<String, HashSet<String[]>> efferent;
	private	HashMap<String, HashSet<String[]>> afferent;
	private	HashSet<String> dependPacks;

	/**
	 * Initialize the compiler.
	 */
	public Project() {
		super("Metrics", JastAddJVersion.getVersion());
		parser = new JavaParser() {
			@Override
				public CompilationUnit parse(java.io.InputStream is,
						String fileName) throws java.io.IOException,
							 beaver.Parser.Exception {
								 return new parser.JavaParser().parse(is, fileName);
							 }
		};
		bytecodeParser = new BytecodeParser();
	}

	/**
	 *	Add options
	 */
	protected void initOptions() {
		super.initOptions();
		Options options = program.options();
		options.addKeyOption("-nostandard");
		options.addKeyOption("-graph");
		options.addKeyOption("-list");
		options.addKeyOption("-silent");
	}

	/**
	 * Run the compiler.
	 * @param args command-line arguments
	 * @return 0 on success, 1 on error, 2 on configuration error, 3 on system
	 */

	public int run(String args[]) {
		int result = run(args, bytecodeParser, parser);
		noStandard = program.options().hasOption("-nostandard");
		checkCouplings();
		calculateMetrics();
		if (program.options().hasOption("-graph")) {
			printGraph();
		}
		if (program.options().hasOption("-list")) {
			printList();
		}
		return result;
	}

	/**
	 * Computes couplings based on collections from
	 * compilation units.
	 */

	private void checkCouplings() {
		String pack;
		absPackages = new HashMap<String, LinkedList<Boolean>>();
		packages = new HashMap<String, LinkedList<String>>();
		efferent = new HashMap<String, HashSet<String[]>>(); 
		afferent = new HashMap<String, HashSet<String[]>>();
		dependPacks = new HashSet<String>();
		for (CompilationUnit c : program.getCompilationUnits()) {	
			try {
				pack = c.getPackageDecl();
				addClasses(pack, c);
				addEfferent(pack, c);
				addAfferent(pack, c);
			} catch (Throwable t) {
				System.err.println("Warning: exception thrown while processing "
						+ c.getClassSource().pathName());
				t.printStackTrace();
			}
		}
	}

	private boolean addEfferent(String pack, CompilationUnit c) {
		if (c.efferentCouplings().size() == 0)
			return false;

		String split[];
		HashSet<String[]> couplings;
		for (String s : c.efferentCouplings()) {
			split = s.split("\\-");
			if(!efferent.containsKey(pack))
				efferent.put(pack, new HashSet<String[]>());
			couplings = efferent.get(pack);
			couplings.add(split);
			dependPacks.add(split[2]);
			efferent.put(pack, couplings);

		}
		return true;
	}


	private boolean addAfferent(String pack, CompilationUnit c) {
		if (c.afferentCouplings().size() == 0)
			return false;

		String split[];
		HashSet<String[]> couplings;
		for (String s : c.afferentCouplings()) {
			split = s.split("\\-");
			if(!afferent.containsKey(pack))
				afferent.put(pack, new HashSet<String[]>());
			couplings = afferent.get(pack);
			couplings.add(split);
			afferent.put(pack, couplings);
		}
		return true;
	}

	private void addClasses(String pack, CompilationUnit c) {
		if(!absPackages.containsKey(pack))
			absPackages.put(pack, new LinkedList<Boolean>());
		absPackages.get(pack).addAll(c.abstractClasses());
		if(!packages.containsKey(pack))
			packages.put(pack, new LinkedList<String>());
		packages.get(pack).addAll(c.classes());
	}
	/**
	 * Calculates metrics and prints the results
	 * to standard out.
	 */


	private void calculateMetrics() {
		Set<String> packSet = packages.keySet();
		boolean silent = program.options().hasOption("-silent");
		for (String s : packSet) {
			if (!silent) {
				System.out.println("Package " + s);
			}
			if (efferent.get(s) != null && !silent) {
				System.out.println("Efferent couplings:");
				String coupling = "";
				for (String split[] : efferent.get(s)) {
					coupling = "		" + split[0] + " depends on " + split[1] + " in package " + split[2];
					System.out.println(coupling);
				}
			}
			if (afferent.get(s) != null && !silent) {
				System.out.println("Afferent couplings:");
				String coupling = "";
				for (String split[] : afferent.get(s)) {
					coupling = "		" + split[1] + " in package " + split[2] + " depends on " + split[0];
					System.out.println(coupling);
				}
			}
			//Calculate instability.
			if (efferent.get(s) == null && afferent.get(s) == null) {
				if (!silent) {
					System.out.println("No efferent or afferent couplings for package " + s + ".");
				}
				instability = 0;
			} else if (efferent.get(s) == null) {
				//Calculated as efferent/(afferent + efferent. Thus always zero)
				instability = 0;
			} else if (afferent.get(s) == null)
				instability = 1;
			else {
				instability = (double)efferent.get(s).size() / (double)(efferent.get(s).size() + afferent.get(s).size());
			}
			DecimalFormat df = new DecimalFormat("0.000");
			DecimalFormatSymbols sym = new DecimalFormatSymbols();
			sym.setDecimalSeparator('.');
			df.setDecimalFormatSymbols(sym);
			//Don't use comma
			if (!silent) {
				System.out.println("\nInstability = " + df.format(instability));	
			}

			//Calculate abstractness.
			double abs = 0;
			double classes = 0;
			LinkedList<Boolean> list = absPackages.get(s);
			classes = list.size();
			for (Boolean b : list)
				abs = b ? abs + 1 : abs;
			abstractness = abs/classes;	
			if (!silent) {
				System.out.println("Abstractness = " + df.format(abstractness));
			}
			//Calculate distance from main sequence.
			if (!silent) {
				System.out.println("Distance from main sequence = " + df.format(Math.abs(abs + instability - 1)/Math.sqrt(2)) + "\n");
			}
		}
	}

	/**
	 * Prints .dot files representing class or
	 * package dependencies based on 'options'.
	 */

	private void printGraph() {
		DotMaker dot = new DotMaker(packages, efferent);	
		dot.genDotPackageFile("packages.dot");	// Hardcode
		dot.genDotClassFile("classes.dot");	// filenames.
	}

	private void printList() {
		DotMaker dot = new DotMaker(packages, efferent);
		dot.genSimplePackageFile("list");
	}

	@Override
	protected int processCompilationUnit(CompilationUnit unit) {
		// Do nothing here - all couplings are instead computed in checkCouplings().
		return EXIT_SUCCESS;
	}
}
