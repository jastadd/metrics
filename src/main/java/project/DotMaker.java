/* Copyright (c) 2014, Olle Tervalampi-Olsson <dat11ote@student.lu.se>,
   Marcus Lacerda <dat11mla@student.lu.se>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Lund University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package project;

import java.util.LinkedList;
import java.util.Set;
import java.util.HashMap;
import java.util.HashSet;

import java.io.FileWriter;
import java.io.File;
import java.io.IOException;

public class DotMaker {

	private	HashMap<String, HashMap<String, LinkedList<String>>> packages;
	private	HashMap<String, HashSet<String[]>> efferent;

	private Set<String> packSet;

	/**
	 * Constructor for the DotMaker, could be reworked
	 * to take a program as parameter instead.
	 * @Param packs the packages collection
	 * @Param eff the efferent collection
	 */

	public DotMaker (HashMap<String, LinkedList<String>> packs, HashMap<String, HashSet<String[]>> eff) {
		packSet = packs.keySet();
		efferent = eff;
		packages = new HashMap<String, HashMap<String, LinkedList<String>>>();
		initPackages(packs);
		processClassDependencies();	
	}

	private void initPackages(HashMap<String, LinkedList<String>> packs) {
		LinkedList<String> classes;
		for (String pack : packSet) {
			packages.put(pack, new HashMap<String, LinkedList<String>>());
			classes = packs.get(pack);
			for (String c : classes)
				packages.get(pack).put(c, new LinkedList<String>());}
	}


	private void processClassDependencies() {
		HashMap<String, LinkedList<String>> classMap;
		HashSet<String[]> effSet;
		for (String pack : packSet) {
			if (!efferent.containsKey(pack))
				continue;
			classMap = packages.get(pack);
			effSet = efferent.get(pack);	
			for (String[] s : effSet) {
				s[0] = formatString(s[0]);
				s[1] = formatString(s[1]);
				classMap.get(s[0]).add(s[1]);
			}
		}
	}
	
	/**
	* Removes class or interface header, something we might want
	* to use later on...
	*/

	private String formatString(String string) {
		if(string.contains(" "))
			return string.split(" ")[1];
		return string;
	}	

	private FileWriter makeFileWriter (String fileName) throws IOException {
		FileWriter w = new FileWriter(new File("graphs/" + fileName));
		return w;
	}

	private void writeHead(FileWriter w) throws IOException {
		w.write("digraph {\n\n");
		w.flush();
	}

	private void close(FileWriter w) throws IOException {
		w.write("}");
		w.flush();
		w.close();
	}

	/**
	 * Generates a .dot file showing dependencies
	 * at package level, not showing any classes.
	 */ 

	public void genDotPackageFile(String fileName) {
		try {
			FileWriter pw = makeFileWriter(fileName);
			writeHead(pw);
			pw.write("\tnode [shape = square]\n");

			for (String pack : packSet) {
				pw.write(weighPackageDependencies(pack));
			}
			pw.write("\n");
			close(pw);

		} catch (IOException e) { e.printStackTrace(); }
	}

	private String weighPackageDependencies (String pack) {
		String weighted = "";
		HashMap<String, Integer> couplings = new HashMap<String, Integer>();
		if (!efferent.containsKey(pack))
			return weighted;
		HashSet<String[]> couplingSet = efferent.get(pack);
		for (String[] s : couplingSet) {
			if (!couplings.containsKey(s[2]))
				couplings.put(s[2], 0);
			couplings.put(s[2], couplings.get(s[2]) + 1);
		}
		Set<String> set = couplings.keySet();
		for (String s : set) {
			weighted += "\t" + format(pack) + " -> " + format(s) + " [label = " + couplings.get(s) + "]\n";
		}
		return weighted;
	}

	private String format(String pack) {
		return pack.replace('.','_');
	}

	/**
	 * Generates a .dot file showing dependencies
	 * between classes in different packages.
	 */

	public void genDotClassFile(String fileName) {	
		try {
			FileWriter cw = makeFileWriter(fileName);
			writeHead(cw);
			for (String pack : packSet)
				cw.write(genClassDeps(pack));
			cw.write("\n");		
			for (String pack : packSet)
				cw.write(genSubgraph(pack));
			close(cw);

		} catch (IOException e) { e.printStackTrace(); }
	}

	private String genClassDeps(String pack) {
		String classDeps = "";
		HashMap<String, LinkedList<String>> classMap = packages.get(pack);
		Set<String> classSet = classMap.keySet();

		for (String c : classSet) {
			LinkedList<String> depList = classMap.get(c);
			if(depList.isEmpty())
				continue;
			classDeps += "\t" + c + " -> { ";
			int length = depList.size();
			for (int i = 0; i < length - 1; i++) {
				classDeps += depList.get(i) + ", ";

			}
			classDeps += depList.get(length - 1) + " }\n";
		}
		return classDeps;
	}

	private String genSubgraph(String pack) {
		String fpack = format(pack);
		
		String head = "\tsubgraph cluster_" + fpack + " {\n";
		String label = "\t\tlabel = \"" + fpack + "\";\n\t}\n\n";
		String classes = "\t\t";

		Set<String> classSet = packages.get(pack).keySet();
		for (String c : classSet) {
			classes += c + "; ";
		}
		classes += "\n";

		return head + classes + label;
	}

	/**
	 * Generates a package dependency representation
	 * on the from:
	 * pack1 pack2	weight
	 * Weight is an int.
	 */

	public void genSimplePackageFile(String fileName) {
		try {
			FileWriter pw = makeFileWriter(fileName);
			for (String pack : packSet) {
				pw.write(simplePackageDependencies(pack));
			}
			pw.flush();
			pw.close();

		} catch (IOException e) { e.printStackTrace(); }
	}

	private String simplePackageDependencies (String pack) {
		String weighted = "";
		HashMap<String, Integer> couplings = new HashMap<String, Integer>();
		if (!efferent.containsKey(pack))
			return weighted;
		HashSet<String[]> couplingSet = efferent.get(pack);
		for (String[] s : couplingSet) {
			if (!couplings.containsKey(s[2]))
				couplings.put(s[2], 0);
			couplings.put(s[2], couplings.get(s[2]) + 1);
		}
		Set<String> set = couplings.keySet();
		for (String s : set) {
			weighted += pack + " " + s + "\t" + couplings.get(s) + "\n";
		}
		return weighted;
	}

/*	public void genSimpleClassFile(String fileName) {	
		try {
			FileWriter cw = makeFileWriter(fileName);
			for (String pack : packSet)
				cw.write(genClassDeps(pack));
			cw.write("\n");		
			for (String pack : packSet)
				cw.write(genSubgraph(pack));
			close(cw);

		} catch (IOException e) { e.printStackTrace(); }
	} */
}
