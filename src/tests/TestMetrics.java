import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;
import java.io.PrintStream;
import java.util.Scanner;
import java.util.ArrayList;
public class TestMetrics {

	private static String SYS_LINE_SEP = System.getProperty("line.separator");

	public static void main(String[] args) {
		TestMetrics t = new TestMetrics();
	}
	public TestMetrics() {
		String PROJECT_DIR = "";
		try {
			PROJECT_DIR = new java.io.File(".").getCanonicalPath();
			String TEST_DIR = PROJECT_DIR + "/tests/autotests";
			File PROJECT = new File(PROJECT_DIR);
			String jarpath = "";
			for (File f : PROJECT.listFiles()) {
				if (f.getPath().contains(".") && f.getPath().split("\\.")[1].equals("jar")) {
					jarpath = f.getAbsolutePath();
					break;	
				}
			}
			File TESTS = new File(TEST_DIR);
			for (File testCase : TESTS.listFiles()) {
				System.out.println("Running test: " + testCase.getName());
				if (testCase.isDirectory()) {
					runTest(PROJECT_DIR, testCase, jarpath);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}


	}
	public void runTest(String MAIN_DIR, File testCase, String jarpath) {
		File out = new File(testCase.getAbsolutePath() + "/output.out");
		File expected = null;
		ArrayList<File> programFiles = new ArrayList<File>();
		for (File f : testCase.listFiles()) {
			if (f.getPath().contains(".")); {
				switch(f.getPath().split("\\.")[1]) {
					case "java":
						programFiles.add(f);
						break;
					case "out":
						out = f;
						break;

					case "expected":
						expected = f;
						break;
					default:
						break;
				}
			}
		}
		//Find project jar file
		String run = "java -jar " + jarpath; 
		for (File f : programFiles) {
			run = run + " " + f.getAbsolutePath();
		}
		try {
			ProcessBuilder p = new ProcessBuilder(run.split(" "));
			p = p.redirectOutput(out);
			Process proc = p.start();
			proc.waitFor();
			if (!(expected == null)) {
				if (!(out.canWrite())) {
					out.setWritable(true);
				}
				StringBuilder s = compareOutput(out, expected);
				if (s.toString().equals("")) {
					System.out.println("Testcase " + testCase.getName() + " passed");
				} else {
					System.out.println(s.toString());
				}
			} else {
				System.out.println("No expected file found for test suite: " + testCase.getName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private StringBuilder compareOutput(File out, File expected) {
		StringBuilder diffs = new StringBuilder();
		try {
			BufferedReader outReader = new BufferedReader(new FileReader(out));
			BufferedReader expectedReader = new BufferedReader(new FileReader(expected));
			String line1 = outReader.readLine();
			String line2 = expectedReader.readLine();
			while (line1 != null || line2 != null) {
				if (line1 == null) {
					diffs.append("Extra line in " + expected.getName() + "\n" + line2 + "\n");
					line2 = expectedReader.readLine();
				} else if (line2 == null) {
					diffs.append("Extra line in " + out.getName() + "\n" + line1 + "\n");
					line1 = outReader.readLine();
				} else {
					if (!line1.equals(line2)) {
						diffs.append("Error: \nExpected: " + line2 + "\n" + "Actual: " + line1 + "\n");
					}
					line1 = outReader.readLine();
					line2 = expectedReader.readLine();
				}
			} 
		} catch (IOException e) {
			e.printStackTrace();
		}
		return diffs;
	}
	private static String normalizeText(String text) {
		return text.replace(SYS_LINE_SEP, "\n").trim();
	}

}
